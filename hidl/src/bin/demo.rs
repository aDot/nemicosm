use anyhow::Result;
use fs_err as fs;

fn main() -> Result<()> {
    let tree = fs::read_to_string("hypercosm.hidl")?;

    let tree = hidl::grammar::NamespaceParser::new().parse(&tree).unwrap();
    let tree = hidl::hir::lower_namespace(tree);

    debug2::dbg!(&tree);

    let json = serde_json::to_string_pretty(&tree)?;

    fs::write("hypercosm.json", &json)?;
    Ok(())
}
