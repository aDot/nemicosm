use serde::{Deserialize, Serialize};

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Namespace {
    pub name: String,
    pub interfaces: Vec<Interface>,
    pub types: Vec<TypeDef>,
    pub extensions: Vec<Extension>,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]

pub struct Extension {
    pub name: String,
    pub version: Version,
    pub interface: Option<ImplicitInterface>,
    pub interfaces: Vec<ExtensionInterface>,
    pub types: Vec<TypeDef>,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]

pub struct Interface {
    pub name: String,
    pub version: Version,
    pub methods: Vec<Func>,
    pub events: Vec<Func>,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ImplicitInterface {
    // Gets version and name from extension
    pub methods: Vec<Func>,
    pub events: Vec<Func>,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ExtensionInterface {
    pub name: String,
    // May get version from extension
    pub version: Option<Version>,
    pub methods: Vec<Func>,
    pub events: Vec<Func>,
}

pub type Version = (u8, u8, u8);

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Func {
    pub name: String,
    pub args: Vec<Arg>,
    pub ret: Option<Type>,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]

pub enum Type {
    Primitive(PrimType),
    Custom(String),
    Array(Box<Type>),
    IntType(IntType),
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]

pub enum PrimType {
    String,
    Object,
    Uuid,
    Bytes,
    Bool,
    Matrix4x4,
    F32,
    F64,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum IntType {
    U8,
    U16,
    U32,
    U64,
    U128,
    VU8,
    VU16,
    VU32,
    VU64,
    VU128,
    I8,
    I16,
    I32,
    I64,
    I128,
    VI8,
    VI16,
    VI32,
    VI64,
    VI128,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]

pub struct Arg {
    pub name: String,
    pub ty: Type,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct TypeDef {
    pub name: String,
    pub kind: TypeKind,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]

pub enum TypeKind {
    Struct(Struct),
    Enum(Enum),
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Enum {
    pub backing: IntType,
    pub fields: Vec<EnumField>,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct EnumField {
    pub name: String,
    pub value: Option<i64>,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Struct {
    pub fields: Vec<StructField>,
}

#[derive(Debug, debug2::Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct StructField {
    pub name: String,
    pub ty: Type,
}
