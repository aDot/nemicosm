struct Object {
    handler: Handler,
}

type Handler = fn(u32, &[u32]);
