trait Object {
    fn id(&self) -> u64;

    fn interfaces(self) -> Vec<String>;

    fn release(self);
}
