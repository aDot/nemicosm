use bytes::Bytes;

use crate::num::{Vi32, Vu64};

type ObjId = Vu64;

struct Header {
    compression: Compression,
    wire_lenght: Vu64,
}

struct Call {
    header: Header,
    /// Identifies what call/responce this is part of
    ///
    /// LSB always 0
    sequence: Vu64,
    /// What function to execute
    command_number: Vi32,
    /// What object to execute it on
    object: ObjId,
    /// Serialized arguments to the function
    data: Bytes,
}

struct Responce {
    header: Header,
    /// Identifies what call/responce this is part of
    ///
    /// LSB always 1
    sequence: Vu64,
    /// Result of the call
    status: Vi32,
    /// Serialized result of the call
    data: Bytes,
}

#[repr(u8)]
enum Compression {
    None = 0,
}
